﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using System.Collections;
using System.Runtime.InteropServices;
using AIDLL;
using System.Reflection;
using System.IO;

namespace AIDLL
{
    public class AIClass : DLLInterface
    {

        public enum STATE { Invalid = 0, Dead, MoveTowards, MoveAway, circleAround }

        List<UnitDataStore> ownTeam = new List<UnitDataStore>();
        List<UnitDataStore> opponentsTeam = new List<UnitDataStore>();
        List<PlanetData> planets = new List<PlanetData>();

        Vector2 midPoint = new Vector2(0, 0);
        Vector2 topCorner;
        Vector2 botCorner;

        float wallAvoidDistance = 5.0f;
        float planetAvoidDistance = 5.0f;

        bool firstTime = true;


        public void SendData(object[] gameUnitData, int team, int ownTeamScore, int otherTeamScore, float timeLeft, Vector2 topRigtCorner, Vector2 bottomLeftCorner, object[] pLanetData, Action<object[]> callBack, Action commandsFinished)
        {

            topCorner = topRigtCorner;
            botCorner = bottomLeftCorner;

            if (firstTime)
            {
                firstTime = false;
                Initialize(gameUnitData, team);
                Initialize(pLanetData);
                return;
            }
            else
            {
                UpdateUnitData(gameUnitData, team);
            }

            FindClosestUnit();

            SurroundClosestUnit(callBack);



            //object[] test = { unitId, newSpeed, newRotarion, newRotarionSpeed, willFire, fireTarget };
            commandsFinished();
        }

        /// <summary>
        /// finds unit that is closest to opponents unit and tags them for targeting
        /// </summary>
        private void FindClosestUnit()
        {
            UnitDataStore ownTempUnit = null;
            UnitDataStore opponentTempUnit = null;
            float distance = 0;
            foreach (UnitDataStore ownUnit in ownTeam)
            {
                //finds ownunit that is closest to enemy unit
                foreach (UnitDataStore opponentUnit in opponentsTeam)
                {
                    if (opponentTempUnit == null)
                    {
                        opponentTempUnit = opponentUnit;
                        ownTempUnit = ownUnit;
                        distance = Vector2.Distance(ownUnit.gameUnitData.position, opponentUnit.gameUnitData.position);
                    }
                    else
                    {
                        if (Vector2.Distance(ownUnit.gameUnitData.position, opponentUnit.gameUnitData.position) <= distance)
                        {
                            ownTempUnit = ownUnit;
                            opponentTempUnit = opponentUnit;
                            distance = Vector2.Distance(ownUnit.gameUnitData.position, opponentUnit.gameUnitData.position);
                        }
                    }
                }
            }
            ownTeam.Single(x => x == ownTempUnit).CommnadUnit = true;
            opponentsTeam.Single(x => x == opponentTempUnit).targeted = true;
        }

        private void SurroundClosestUnit(Action<object[]> callBack)
        {
            UnitDataStore opponentUnit = opponentsTeam.Single(x => x.targeted == true);
            bool leftSide = true;
            foreach(UnitDataStore unitData in ownTeam)
            {
                float rotationTarget = unitData.gameUnitData.rotateTarget;
                Vector2 direction = new Vector2();
                bool willFire = true;

                //Firing target
                Vector2 targetDirection = GetXYDirection(opponentUnit.gameUnitData.angle, opponentUnit.gameUnitData.maxSpeed);
                Vector2 fireTarget = CalculateInterceptCourse(opponentUnit.gameUnitData.position, targetDirection, unitData.gameUnitData.position,5);//5 is bullet speed
                if (AvoidWalls(unitData, out direction) == false)// awoiding walls is highest priority
                {
                    if (AvoidPlanets(unitData, out direction) == false)
                    {
                        if (Vector2.Distance(unitData.gameUnitData.position, opponentUnit.gameUnitData.position) < unitData.gameUnitData.firingRange * 1)
                        {
                            direction = opponentUnit.gameUnitData.position - unitData.gameUnitData.position;
                            rotationTarget = Vector2.Angle(new Vector2(0, 1), direction);
                            if (direction.x > 0)
                            {
                                rotationTarget = 360 - rotationTarget;
                            }
                            if (leftSide)//joku tapa muistaa tämä booli updatejen yli
                            {
                                rotationTarget += 80;
                                leftSide = false;
                            }
                            else
                            {
                                rotationTarget -= 80;
                                leftSide = true;
                            }
                        }
                        else
                        {
                            direction = opponentUnit.gameUnitData.position - unitData.gameUnitData.position;
                            rotationTarget = Vector2.Angle(new Vector2(0, 1), direction);
                            if (direction.x > 0)
                            {
                                rotationTarget = 360 - rotationTarget;
                            }
                            rotationTarget += 10;


                        }
                    }
                    else
                    {
                        rotationTarget = Vector2.Angle(new Vector2(0, 1), direction);
                        if (direction.x > 0)
                        {
                            rotationTarget = 360 - rotationTarget;
                        }
                    }
                }
                else
                {
                    direction = midPoint - unitData.gameUnitData.position;
                    rotationTarget = Vector2.Angle(new Vector2(0, 1), direction);
                    if (direction.x > 0)
                    {
                        rotationTarget = 360 - rotationTarget;
                    }
                }
                object[] parametres = { unitData.gameUnitData.id, unitData.gameUnitData.maxSpeed, rotationTarget, unitData.gameUnitData.maxRotateSpeed, willFire, fireTarget };
                callBack(parametres);
            }
        }

        private bool AvoidPlanets(UnitDataStore unitData, out Vector2 direction)
        {
            foreach(PlanetData planet in planets)
            {
                if (Vector2.Distance(unitData.gameUnitData.position, planet.position) - planet.radius < planetAvoidDistance)
                {
                    direction = unitData.gameUnitData.position - planet.position;
                    return true;
                }
            }


            direction = new Vector2();
            return false;
        }

        private bool AvoidWalls(UnitDataStore unitData ,out Vector2 direction)
        { 
            bool avoiding = false;
            direction = new Vector2();

            if( Mathf.Abs(unitData.gameUnitData.position.x - topCorner.x) < wallAvoidDistance ||
                Mathf.Abs(unitData.gameUnitData.position.x - botCorner.x) < wallAvoidDistance ||
                Mathf.Abs(unitData.gameUnitData.position.y - topCorner.y) < wallAvoidDistance ||
                Mathf.Abs(unitData.gameUnitData.position.y - botCorner.y) < wallAvoidDistance)
            {
                avoiding = true;
            }

            return avoiding;
        }

        /// <summary>
        /// Populates unit lists
        /// </summary>
        /// <param name="gameUnitData"></param>
        private void Initialize(object[] gameUnitData, int team)
        {
            List<GameUnitData> list = new List<GameUnitData>();

            foreach (object o in gameUnitData)
            {
                list.Add(o as GameUnitData);
            }

            foreach (GameUnitData unit in list)
            {
                if (unit.side == team)
                {
                    ownTeam.Add(new UnitDataStore(unit.id,STATE.Invalid,unit));
                }
                else
                {
                    opponentsTeam.Add(new UnitDataStore(unit.id, STATE.Invalid, unit));
                }
            }
        }

        private void Initialize(object[] planetData)
        { 
            foreach(PlanetData planet in planetData)
            {
                planets.Add(planet);
            }
           
        }

        private void UpdateUnitData(object[] gameUnitData, int team)
        {
            foreach (UnitDataStore unit in ownTeam)
            {
                unit.CommnadUnit = false;
            }
            foreach (UnitDataStore unit in opponentsTeam)
            {
                unit.targeted = false;
            }
            List<GameUnitData> list = new List<GameUnitData>();
            foreach (object o in gameUnitData)
            {
                list.Add(o as GameUnitData);
            }
            foreach (GameUnitData unit in list)
            {
                if (unit.side == team)
                {
                    ownTeam.Single(x => x.id == unit.id).gameUnitData = unit;
                }
                else
                {
                    opponentsTeam.Single(x => x.id == unit.id).gameUnitData = unit;
                }
            }

        }
        private Vector2 GetXYDirection(float angle, float magnitude) 
        {
            angle += 90;
            return new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle)) * magnitude;
        }

        /*
        private Vector2 CalculateInterceptCourse(Vector2 aTargetPos, Vector2 aTargetSpeed, Vector2 shooterPos, float bulletSpeed)
        {
            Vector2 targetDir = aTargetPos - shooterPos;

            float iSpeed2 = bulletSpeed * bulletSpeed;
            float tSpeed2 = aTargetSpeed.sqrMagnitude;
            float fDot1 = Vector3.Dot(targetDir, aTargetSpeed);
            float targetDist2 = targetDir.sqrMagnitude;
            float d = (fDot1 * fDot1) - targetDist2 * (tSpeed2 - iSpeed2);
            if (d < 0.1f)  // negative == no possible course because the interceptor isn't fast enough
            {
                Debug.LogError(1);
                return Vector3.zero;
            }
            float sqrt = Mathf.Sqrt(d);
            float S1 = (-fDot1 - sqrt) / targetDist2;
            float S2 = (-fDot1 + sqrt) / targetDist2;

            if (S1 < 0.0001f)
            {
                if (S2 < 0.0001f)
                {
                    Debug.LogError(2);
                    return Vector3.zero;
                }
                else
                {
                    return (S2) * targetDir + aTargetSpeed + shooterPos;
                }
            }
            else if (S2 < 0.0001f)
            {
                return (S1) * targetDir + aTargetSpeed + shooterPos; ;
            }
            else if (S1 < S2)
            {
                return (S2) * targetDir + aTargetSpeed + shooterPos; ;
            }
            else
            {
                return (S1) * targetDir + aTargetSpeed + shooterPos; ;
            }
        }
        */
        private Vector2 CalculateInterceptCourse(Vector2 aTargetPos, Vector2 aTargetSpeed, Vector2 shooterPos, float bulletSpeed)
        {
            Vector2 targetVelocityVector = new Vector2(0, 0) * aTargetSpeed.magnitude;
            Vector2 interceptVelocityVector = new Vector2(0, 0) * bulletSpeed;


            Vector2 totarget = aTargetPos - shooterPos;

            Vector2 vr = targetVelocityVector - interceptVelocityVector;
            float a = Vector2.Dot(vr, vr) - (bulletSpeed * bulletSpeed);
            float b = 2 * Vector2.Dot(vr, totarget);
            float c = Vector2.Dot(totarget, totarget);
            float p = -b / (2 * a);
            
            if ((b * b) - 4 * a * c < 0)
            {
                return Vector2.zero;
            }
            float q = (float)Math.Sqrt((b * b) - 4 * a * c) / (2 * a);

            float t1 = p - q;
            float t2 = p + q;
            float t;

            if (t1 > t2 && t2 > 0)
            {
                t = t2;
            }
            else
            {
                t = t1;
            }

            Vector2 aimSpot = aTargetPos + aTargetSpeed * t;
            Vector2 bulletPath = aimSpot - shooterPos;
            float timeToImpact = bulletPath.magnitude / bulletSpeed;//speed must be in units per second

            return aimSpot;
        }
        /*
        private Vector2 CalculateInterceptCourse(Vector2 aTargetPos, Vector2 aTargetSpeed, Vector2 shooterPos, float bulletSpeed)
        {
            Vector2 totarget = target.position - tower.position;

            float a = Vector.Dot(target.velocity, target.velocity) - (bullet.velocity * bullet.velocity);
            float b = 2 * Vector.Dot(target.velocity, totarget);
            float c = Vector.Dot(totarget, totarget);

            float p = -b / (2 * a);
            float q = (float)Math.Sqrt((b * b) - 4 * a * c) / (2 * a);

            float t1 = p - q;
            float t2 = p + q;
            float t;

            if (t1 > t2 && t2 > 0)
            {
                t = t2;
            }
            else
            {
                t = t1;
            }

            Vector aimSpot = target.position + target.velocity * t;
            Vector bulletPath = aimSpot - tower.position;
            float timeToImpact = bulletPath.Length() / bullet.speed;//speed must be in units per second
        }
         * */

    }
}
